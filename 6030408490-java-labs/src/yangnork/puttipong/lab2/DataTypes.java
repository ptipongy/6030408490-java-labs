package yangnork.puttipong.lab2;

/*
 * This program is assignment to help student understand basic data type in java
 * Usage: just run
 * author: Puttipong Yangnork
 * ID: 603040849-0
 */


public class DataTypes {
	public static void main(String[] args) {
		
	String NAME = "Puttipong Yangnork"; // My name
	String ID = "6030408490";			// My ID
	
	char theFirstLetterOfYourFirstName = NAME.charAt(0);		// first letter
	
	String theLastTwoDigitsID = ID.substring(ID.length() - 2); 	// last two digit
	String theFirstTwoDigitsID = ID.substring(0,2); 			// first two digit
	
	boolean truth = true; 										// truth is true
	
	String myIdInOctal = "132";
	int idInDecFromOc = Integer.parseInt(myIdInOctal, 8); 		// convert from octal to decimal in Integer
	
	String myIdInHex = "5A";
	int idInDecFromHex = Integer.parseInt(myIdInHex, 16); 		// convert from hexadecimal to decimal in Integer
		
	long idLong = Long.parseLong(theLastTwoDigitsID);			// convert data type form String to Long
	float idFloat = Float.parseFloat(theLastTwoDigitsID + "." + theFirstTwoDigitsID); // convert data type form String to Float 
	double idDouble = Double.parseDouble(theLastTwoDigitsID + "." + theFirstTwoDigitsID); // convert data type form String to Double
	
	String s = " "; // space
	
	System.out.println(" My name is " + NAME);
	System.out.println(" My Student ID is " + ID);
	System.out.println(s + theFirstLetterOfYourFirstName + s + truth + s +  idInDecFromOc + s + idInDecFromHex);
	System.out.println(s + idLong + s + idFloat + s + idDouble);
	
	}
	
}
