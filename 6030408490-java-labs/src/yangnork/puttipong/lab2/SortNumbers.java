package yangnork.puttipong.lab2;

import java.util.Arrays;

/*
 * This program is sorting number program that accept only 5 number argument.
 * Usage: SortNumbers <number1> <number2> <number3> <number4> <number5>
 * 
 * Author: Puttipong Yangnork
 * ID: 603040849-0
 * today is a good day :)
 */

public class SortNumbers {
	public static void main(String[] args) {
		if(args.length != 5) { // argument check
			System.err.println("Usage: SortNumbers <number1> <number2> <number3> <number4> <number5>");
			System.exit(1);
		}
		
		float[] numbers = new float[5]; // define an array with 5 float viable capacity 
		for(int i = 0; i < args.length; i++) { // store numbers in array
			numbers[i] = Float.parseFloat(args[i]);
		}
		
		Arrays.sort(numbers); // utility function to sort array
		System.out.println(Arrays.toString(numbers)); // show array
		
	}
}
