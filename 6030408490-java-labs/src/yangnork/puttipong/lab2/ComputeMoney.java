package yangnork.puttipong.lab2;

/*
 * ComputeMoney is only 4 argument accept program 
 * that compute total money from 4 different type of Thai Banknotes 
 * Usage: ComputeMoney <1,000 baht> <500 baht> <100 baht> <20 baht> 
 * author: Puttipong Yangnork
 * ID: 603040849-0
 */

public class ComputeMoney {
	public static void main(String[] args) {
		if (args.length != 4) { // argument check
			System.err.println("Usage: ComputeMoney <1,000 baht> <500 baht> <100 baht> <20 baht> ");
			System.exit(1);
		}

		double thounsandNote = Double.parseDouble(args[0]); // number of 1000 Baht notes
		double fiveHundredNote = Double.parseDouble(args[1]); // number of 500 Baht notes
		double oneHundredNote = Double.parseDouble(args[2]); // number of 100 Baht notes
		double twentyNote = Double.parseDouble(args[3]); // number of 20 Baht notes

		double totalMoney = 1000 * thounsandNote + 500 * fiveHundredNote 
				+ 100 * oneHundredNote + 20 * twentyNote; // compute total money

		System.out.println("Total Money is " + totalMoney + " Baht");

	}

}
