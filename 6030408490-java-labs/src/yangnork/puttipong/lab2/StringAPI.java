package yangnork.puttipong.lab2;

/*
 * This program is using StringAPI to detect the school name and reply that the school is a college,
 * a university or neither.
 * accept only 1 argument
 * Usage: StringAPI "<School name>"
 * 
 * note: if the name has space, use "" cover the name, example "ban nhong hoi college but not a university".
 * 
 * Author: Puttipong Yangnork
 * ID:603040849-0
 * Gintama release the new episode: Gintama but Prince of Tennis :)  
 */

public class StringAPI {
		
	static int wordCount(String[] string, String word) { //
		int count = 0; // word counter
		for(int i = 0; i < string.length; i++) { 
			if(string[i].equalsIgnoreCase(word)) {
				count = count + 1; // 
			}		
		}
		return count;
	}
	
	public static void main(String[] args) {
		if(args.length != 1) { // argument check
			System.err.println("Usage: StringAPI \"<School name>\" ");
			System.exit(1);
		}
		
		String schoolName = args[0];
		
		String[] splitedName = schoolName.split(" "); // split the school name
		
		int college = wordCount(splitedName, "college"); //
		int university = wordCount(splitedName, "university"); //
		
		if(college > 0 && university > 0) { // the school is both a college and university
			System.out.println(schoolName + " is both a college and a university.");
		} 
		else if(college > 0 && university == 0) { // the school is college 
			System.out.println(schoolName + " is a college.");
			
		}
		else if(college == 0 && university > 0) { // the school is university
			System.out.println(schoolName + " is a university.");			
		}
		else { // not both school and university
			System.out.println(schoolName + " is neither a college nor a university.");
		}
		
	}
}
